# How to speed the auto-deploy process for urgent merge requests?

Specific situations such as mitigating incidents or unblocking the security release,
could require for a specific merge request (most likely a revert) to be deployed as
quickly as possible. Instead of waiting for a new auto-deploy branch to be created,
which could be time-consuming, there are some steps the release manager can follow to
speed up the auto-deploy process:

## For canonical merge requests

1. Cherry-pick the merge request into the latest auto-deploy branch by
   adding `~Pick into auto-deploy` and the `severity::2` or higher labels.
1. Trigger the `MANUAL auto-deploy pick&tag - ⚠️ KEEP INACTIVE!!` [pipeline schedule].

Even if the manual schedule is not activated, the `auto_deploy:timer` will take
case of picking the change and tagging during its regular run (every 15 minutes).

## For security merge requests

This process should be used for critical security releases:

1. Disable the auto-deploy process: `/chatops run auto_deploy pause`
1. Find the current auto-deploy branch. There are several options for this:

    a. On Slack, search for the last message for the auto-deploy branch creation. There should be a link to the GitLab auto-deploy branch:

    ![auto-deploy branch creation](images/auto-deploy-branch-creation.png)

    b. On Slack, search for the last coordinated pipeline created, on the message there should be a link to the auto-deploy branch:

    ![coordinated pipeline creation](images/coordinated-pipeline-creation.png)

    c. Search it on the [GitLab security project]. Branches are sorted by time so the first one is the most recent one.
1. Cherry-pick the security fix into the current auto-deploy branch. This can be done manually or via [cherry-pick UI on the security merge request],
   in the last case **do not select** "Start a new merge request with these changes".
1. Execute the `MANUAL auto-deploy pick&tag - ⚠️ KEEP INACTIVE!!` [pipeline schedule]. This will create a coordinated pipeline with the security fix.
1. To enable the auto-deploy process, wait until the security commit has a green pipeline on the GitLab Security master branch. This guarantees subsequent
   coordinated pipelines include the security fix.

    a. Auto-deploy process can be enabled with: `/chatops run auto_deploy unpause`.

[pipeline schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[GitLab security project]: https://gitlab.com/gitlab-org/security/gitlab/-/branches?state=all&sort=updated_desc&search=auto-deploy
[cherry-pick UI on the security merge request]: https://docs.gitlab.com/ee/user/project/merge_requests/cherry_pick_changes.html#cherry-pick-all-changes-from-a-merge-request
