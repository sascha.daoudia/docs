# Overview

QA smoke and reliable specs are run as part of the auto-deploy pipeline - this means they are run regularly and can be assumed as stable.
A breakdown of specs executed per environment can be seen on the [QA test pipelines] documentation.

This document describes the process to follow if QA tests are failing and blocking deployments.

## Process Overview

Failing QA tests are always tracked using an issue in the [release tracker] to give a record of the failure.
This is a change away from incidents as part of https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/752
 
Quality are here to help us investigate and solve any failures through:

* Maintaining a [Quality on call schedule] to show who to ping in case of QA failures.
* Keeping control of all test results, these are posted in the respective `#qa-<env>` Slack channel.

For context, there's a quality Slack channel per environment:
* `#qa-staging`: List pipelines for `gstg-cny` and `gstg`.
* `#qa-production`: List QA pipelines for `gprd-cny` and `gprd`.
* `#qa-staging-ref`: List QA pipelines for staging-ref.
* `#qa-release`: List QA pipelines associated with the release environment.
* `#qa-preprod`: List QA pipelines associated with the pre environment.

When a QA job fails in any of our environments (`gstg-cny`, `gstg`, `gprd`, `pre` or `release`)

1. Follow the steps in the [Handling Deploy Failures runbook] to create an issue in the [release tracker].
1. Check the `#qa-<env>` Slack channel, the failure may already be known and being worked on.
1. If not known, escalate to the Quality engineer on call (the `#quality` Slack channel can be used for this) and ask
   for assistance on the issue. To know who is the engineer on-call use:
    * Execute `/chatops run quality dri schedule` on Slack, or,
    * Ping the [Quality on call schedule].
1. The Quality engineer on-call will analyze the failure and suggest next steps. Release managers can also help, see the
   "Fixing QA failures from the Release manager side" section below.
1. If the failure appears to be code or environment-related, [declare an incident] with the correct [availability severity] and [Delivery impact] label.
    * Link to the release issue created in step 1.
    * Work with the EoC to determine if the QA failure should block deployments, if so, make sure the `block deployments` label is added to the incident issue.
1. Once tests are passing again:
    * Update the issue with a summary of the failure.
    * Apply the `deploys-blocked-gprd::*` and `deploys-blocked-gstg::*` labels and close the issue.
    * Add an annotation to the [Auto deploy packages dashboard] to track the QA failure.

## Fixing QA failures from the Release manager side.

The following are a list of steps that a release manager can use to
help fixing a broken QA and unblock the auto-deploy process.

**Note**: Quality owns the process of analyzing, determining the root cause and fixing a broken QA spec. If inclined,
Release managers can help to speed up this process, but this is not expected of them.

Broken QA jobs can be caused by:

1. A feature flag.
1. Code that is no longer compatible with the QA spec due to a bug or a change in the feature.
1. QA users being blocked.
1. A environment/infrastructure failure. In this case, an an [incident will need to be reported](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident).

### Feature flag

This is the quickest way to fix a broken QA job. To determine if a QA spec is failing because of
a feature flag:
1. Search for feature flags enabled or disabled around the time of the failures. To search for feature flags
   recently toggled:
    * Take a look at the [feature flag log tracker], or,
    * [Feature flags logs], or
    * Check `#qa-staging` or `#qa-production` Slack channels. End-to-end pipelines
    are triggered when feature flags are toggled. Example

  ![Qa failure example](./images/qa_failure_feature_flag.png)

2. If a feature flag is found, restore the feature flag to the previous state. Optionally, notify the FF author about this,
   but do note their authorization is not required for flipping the feature flag state.
   Switching the feature flag can be done via Slack comand:
   * For staging: `/chatops run feature set <feature_flag_name> <previous_state> --staging`, e.g. `/chatops run feature set foo false --staging`
   * For production: `/chatops run feature set <feature_flag_name> <previous_state>`, e.g. `/chatops run feature set foo false`.
3. Pending the environment, fhe feature flag should be toggled in the `#staging` or the `#production` Slack channel.
4. Retry the QA job.
5. If the QA job succeeds, notify feature flag author and let them know about the problem.
6. If the QA job did not succeed, restore the feature flag to the original state.

## Code is no longer compatible with the QA

In this case, the code should be reverted and re-deployed to the environment to unblock deployments.
Once a merge request has been identified as the culprit:
1. Proceed to revert it. On the merge request widget, click on the `Revert` link and select the default branch.
   This will create a revert merge request

![Revert link](./images/revert_link.png)

2. On the revert merge request, add the `pipeline::revert` label, this will create a shorter pipeline.
3. Go to `#backend_maintainers` or `#frontend_maintainers` Slack channel and ask for an approval
4. Once merged, the merge request will be included in the next auto-deploy branch.
   * Alternatively, you can use `Pick into auto-deploy` to speed up the deployment process.

## A users being blocked

From time to time, QA users are blocked causing the QA specs to fail. To validate if a QA user has been blocked,
open up the QA job that failed and search for the `"You're account has been blocked"` error. Users are automatically unblocked
after some minutes, so in this case, wait for at least 15 minutes and retry the job. If the error persists,
this should be escalated to Quality on-call or the SRE on-call.

Investigation about this failure can be found on the [anti-abuse issue].

## Additional troubleshooting

QA tests are run against the live environments. So if a job is retried from a pipeline, it will be run against whichever version
was most recently deployed to that environment. This means that by retrying a job on an older pipeline, the test code being run will be 
from that pipeline, but the application code deployed to the environment may be from a newer pipeline. A test's success or failure should
only be trusted if it is run against the application code from it's same version.

[QA test pipelines]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures/#qa-test-pipelines
[release tracker]: https://gitlab.com/gitlab-org/release/tasks/-/issues
[Handling Deploy Failures runbook]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/failures.md#handling-deployment-failures
[availability severity]: https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability
[Delivery impact]: https://about.gitlab.com/handbook/engineering/releases/#delivery-impact-labels
[declare an incident]: https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident
[Quality on call schedule]: https://gitlab.com/gitlab-org/quality/pipeline-triage#dri-weekly-rotation-schedule
[feature flag log tracker]: https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/-/issues/?sort=updated_desc&state=closed&first_page_size=20
[Feature flags logs]: https://nonprod-log.gitlab.net/goto/7a9df4f0-6b3e-11ed-9af2-6131f0ee4ce6
[anti-abuse issue]: https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/119
[Auto deploy packages dashboard]: https://dashboards.gitlab.net/d/delivery-auto_deploy_packages/delivery-auto-deploy-packages-information?orgId=1&refresh=5m
