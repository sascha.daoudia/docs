# Overview of the auto-deploy pipelines

The auto-deploy process relies on two [pipelines schedules]:

1. `auto-deploy:timer`
4. `auto-deploy:cleanup`

The code definition of these rake tasks can be found on release-tools, particularly
on the [`auto-deploy.rake`] file.

## `auto-deploy:timer`

This timer is the central piece of auto-deploy, it runs every 15 minutes and 
takes care of the lifecycle of auto-deploy.

It comprises three operations executed on every run:

1. prepare: auto_deploy branch generation
1. pick: processing urgent fixes
1. tag: package tagging

### Prepare

If the current hour is part of [the auto_deploy schedule], and an auto_deploy
branch matching the current time does not yet exists, this task creates 
auto-deploy branches on [GitLab], [Omnibus], and [CNG] security repositories.
The branches are created on security repositories to account for security releases.

To ensure the commits don't overlap, the branches use the previous auto-deploy tag as
the minimum commit. Several auto-deploy branches are created throughout a day, the
schedule is set at the discretion of release managers at the beginning of their shift.
The current schedule can be seen on the [releases documentation].

As an aside, this task also creates the monthly release issue if one can't be found.

### Pick

This task searches for GitLab, Omnibus, and CNG merge requests with the
`Pick into auto-deploy` label, if found, it'll validate they have a severity label 
added and then it'll pick them into the respective auto-deploy branch.
For more details, [see our Releases handbook page].

### Tag

The final task will tag the most recent, non failed commit on the current
auto-deploy branches to build an auto-deploy package. Once a package is tagged, a
coordinated pipeline is created which marks the start of the deployment procedure 
for that tagged package.

In order to start the rollout on the first environment, the pipeline
for the GitLab project is verified to ensure that it is successful now.

### Consideration

When the feature flag [`auto_deploy_tag_latest`] is enabled this forces
the auto-deploy branch to be generated using the latest commit on the
default branch, regardless of the CI status. 
It also forces tagging from the latest commit on the auto_deploy branch,
regardless of the CI status.

This behavior is discouraged as a less risky option exists to [speed up auto-deploy].
This feature flag is disabled by default.

## `auto-deploy:cleanup`

Auto-deploy branches are kept for seven days, branches older than that are deleted
by this task that its executed every day at 12UTC.

## Manual operations

There are two additional inactive [pipelines schedules] intended for manual
operations. The name always begins with _MANUAL auto-deploy_ and then list the
operations included in that specific job.

The one including the _prepare_ operation forces the generation of an auto-deploy
branch unless one for the current hour already exists.

Those jobs are available for release managers in case they need to create a package,
force the generation of a new auto_deploy branch and/or operate when the regular timer
is paused.

[the auto_deploy schedule]: ../general/how-to-set-auto-deploy-branch-schedule.md
[`auto-deploy.rake`]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/auto_deploy.rake
[pipelines schedules]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[releases documentation]: https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-deployments-process
[GitLab]: https://gitlab.com/gitlab-org/security/gitlab
[Omnibus]: https://gitlab.com/gitlab-org/security/omnibus-gitlab
[CNG]: https://gitlab.com/gitlab-org/security/charts/components/images
[`auto_deploy_tag_latest`]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags/191/edit
[speedy deployments]: ../runbooks/how_to_speed_auto_deploy_process_for_urgent_merge_requests.md
[see our Releases handbook page]: https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/#gitlabcom-pick-label
[`auto-deploy.rake`]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/tasks/auto_deploy.rake
[speed up auto-deploy]: ../runbooks/how_to_speed_auto_deploy_process_for_urgent_merge_requests.md