# Release Manager runbook for patch releases

## When to perform a Patch Release?

As a general guideline, [release managers] should periodically review the number of S1 fixes as well as
the overall number of fixes ready to patch (see [patch release pressure]). Two S1 fixes, or 10 overall fixes
are both likely to [trigger a patch], although the final decision rests with the Release Managers. There are
[good reasons not to patch] bugs, especially on older versions.

Before deciding to patch, particularly on versions older than the current stable (for application bugs) consider:
1. Begin by determining how many and the priority of backports waiting a for a patch release (see [patch release pressure])
1. How many users are affected by the bug? Consider how this impacts against the number of users who will need to upgrade to a new release.
1. In case of backport requests, the everity labels must align between the issue, merge request and the backport request.
1. Does the issue have a workaround that can be used instead?
1. How much Release Manager time is a patch going to take? What else could we be doing with that time?
1. Are we close to a major release? Keep in mind that backport requests typically increase after a new major release.

If after considering these questions we decide not to patch make sure the requester knows the reason and
close any open backport requests related to the patch. Otherwise continue to the next step to start the patch release.

## Process to perform a a patch release

### 1. Create an issue to track the patch release

In order to keep track of the various tasks that need to happen before a patch
release is considered "complete", we create an issue on the [release task tracker]
and update it as we progress.

1. A release task issue can be created by executing `/chatops run release prepare` on the `#f_upcoming_release` Slack channel
    * This will create an issue to complete a patch release for the current version and a blog post with the unreleased merge requests
      on the [www-gitlab-com] repository.
    * To create a  patch release issue for an older version:
        * Execute: `/chatops run release prepare <VERSION>`, example: `/chatops run release prepare 11.10.3`
1. You may want to **bookmark** the issue until it's closed at the end of the
   release cycle.

### 2. Complete the patch release tasks

Use the patch issue created earlier to keep track of the process and mark off
tasks as you complete them. The patch issue covers the required steps to tag, deploy and publish the patch release.

## List bug fixes waiting to be released

Release managers can make usage of a ChatOps command to fetch the list of backports waiting to be released. For this:

1. Release managerse can execute the `/chatops run release pending_backports` command on Slack.
1. Release-tools will fetch the unreleased merge requests merged into the last three active stable branches.
1. A message will be posted on Slack listing the merge requests that will be included in the next patch or
security release

| Example |
| ---- |
| ![pending backports](images/pending_backports.png) |

## Stable branches configuration

Stable branches are configured to respect the [maintenance policy] for bug fixes:

* Engineers can backport bug fixes into the current stable branch following the [runbook for GitLab engineers].
* Stable branches outside the [maintenance policy] for bug fixes are restricted to release managers. GitLab
  engineers require a [backport request] to be approved by release managers to backport a high-priority bug
  fix into an older stable branch.

Stable branches configuration[^1] consists of:

1. A protected branch rule for the current stable branch allowing `merge` access to maintainers.
1. A wildcard that covers stable branches limiting the `push` and `merge` access to release managers.
1. A merge request approval rule that enforces maintainer approval on merge requests targeting stable branches[^2].

| [Protected branch rules] | [Merge request approval rules] |
| ----- | ----- |
| ![protected branch](images/protected_branch_rules.png) | ![merge request approval rules](images/merge_request_approval_rules.png) |


Stable branches permissions are adjusted by release managers at the end of each release.

[^1]: Rules 1 and 2 are configured on GitLab and Omnibus projects, the remaining projects under [Managed Versioning] allow maintainers to merge into any stable branch.
[^2]: This rule is only implemented on the [GitLab project]

[release managers]: https://about.gitlab.com/community/release-managers/
[patch release pressure]: patch_release_pressure.md
[trigger a patch]: #1-create-an-issue-to-track-the-patch-release
[good reasons not to patch]: https://docs.gitlab.com/ee/policy/maintenance.html#patch-releases
[release task tracker]: https://gitlab.com/gitlab-org/release/tasks/-/issues
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
[Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[runbook for GitLab engineers]: ./engineers.md
[backport request]: https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases
[Protected branch rules]: https://gitlab.com/gitlab-org/gitlab/-/settings/repository
[Merge request approval rules]: https://gitlab.com/gitlab-org/gitlab/-/settings/merge_requests
[Managed Versioning]: components/managed-versioning/index.md#components-under-managed-versioning
[GitLab project]: https://gitlab.com/gitlab-org/gitlab/
