# Patch release runbook for GitLab Engineers

## General guidelines

As defined on the [Maintenance Policy], **patch releases can only include
bug fixes for the current stable released version of GitLab**.

Bug fixes can be backported to the current version if:

* They have been previously fixed and merged into the GitLab default branch.
* They have been deployed to GitLab.com.
* They are bug fixes. Per the maintenance policy [patch release guidelines], features can't be backported to previous versions.

Patch releases are limited to the current stable released version, however, exception cases may be considered. See the 
[backporting to versions outside of the Maintenance Policy] documentation for more information.

## Backporting a bug fix in the [GitLab project]

To backport a bug fix in the current version, GitLab engineers should:

1. Ensure the bug fixes comply with the [general guidelines]: The original merge request is a bug fix that has been deployed to GitLab.com.
1. Open up a merge request in the [GitLab canonical project], the merge request should target the stable branch associated with the current version.
   * **Note**: For efficiency, a merge request can be created via UI using the [cherry-pick feature].
1. Use the [stable branch template] and follow the check list.
1. Ensure the merge request is approved by a maintainer. Merge requests targeting stable branches only require one approval.
1. Add a [severity label] to the merge request (if applicable). Severity labels helps release managers assess the urgency of
   a patch release.
1. Set the milestone of the merge request to match the target backport branch version.
1. Ensure the [`e2e:package-and-test-ee` pipeline] is executed. This pipeline executes end-to-end testing for the GitLab platform
   guaranteeing the code changes are compliant from the Quality side. This pipeline is automatically executed on merge requests targeting
   stable branches.
1. Verify the `e2e:package-and-test-ee` is successfully executed. If failures arise, contact a Software in Test (SET) engineer to
   review if the failure is related to the merge request.

### Process for maintainers

When a merge request targeting a stable branch is assigned to maintainers, they should:

1. Confirm the merge request being backported has been deployed to GitLab.com
1. Verify only bug fixes are being backported. Per the [Maintenance Policy], features can't be backported.
1. Ensure the [`e2e:package-and-test-ee` pipeline] has been executed.
1. Verify if there are failures on the `e2e:package-and-test-ee` pipeline. Due to [test flakiness], this
   pipeline is allowed to fail, and as a consequence, it won't cause a failure on the upstream GitLab pipeline.
   Failures on this pipeline must be reviewed by a Software Engineer in Test (SET).
   **Don't merge the backport until a SET has confirmed the failures are not related.**
1. Once the above conditions are met, approve and set the merge request to MWPS.

## Backporting a bug fix on [Managed Versioning] projects

Once it has been deemed a bug fix should be backported, engineers should:

1. Ensure the bug fixes comply with the [general guidelines]: The original merge request is a bug fix that has been deployed to GitLab.com.
1. Open up a merge request in the respective canonical project. The merge request should target the stable branch associated with the current version.
1. Use the stable branch template and follow the checklist
   * [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/.gitlab/merge_request_templates/Stable%20Branch.md)
   * [CNG](https://gitlab.com/gitlab-org/build/CNG/-/blob/master/.gitlab/merge_request_templates/Stable%20Branch.md)
1. Ensure the merge request is approved by a maintainer. Merge requests targeting stable branches only require one approval.
1. Add a [severity label] to the merge request (if applicable). Severity labels helps release managers assess the urgency of
   a patch release.

### Process for maintainers

When a merge request targeting a stable branch is assigned to maintainers, they should:

1. Ensure the merge request being backported has been deployed to GitLab.com
1. Verify only bug fixes are being backported. Per the [Maintenance Policy], features can't be backported.
1. Once the above conditions are met, proceed to merge the merge request.

## Additional considerations

1. Release Managers will create patch releases for the current version depending on demand and other release pressures. If a patch
   release for an older version is required, take a look at the [Backporting to versions outside of the Maintenance Policy] section.
1. Backporting outside the [Maintenance Policy] will require release managers' approval, see [Backporting to versions outside of the Maintenance Policy] for details.
1. Keep an eye to the [#releases] and [#development] Slack channels. When a patch release is published, a notification is sent to these channels
   to broadcast the patch release.
1. Delivery is working on generating [long lived environments] to be continuously deployed from stable branches,
   as part of this effort, on the a downstream pipeline called `start-release-environments-pipeline` is automatically
   triggered on commits on stable branches on the [GitLab canonical project], this one is allowed to fail, and such,
   failures can be temporarily ignored.

## Backporting to versions outside of the Maintenance Policy

Non-security patches that are outside of our [Maintenance Policy] must be requested
and agreed upon by the Release Managers and the requester. Read through the [backporting to older releases]
documentation and open up a [backport request] issue if the bug fix meets the criteria.

## Feedback and questions

Reach out the Delivery group on the [#releases] Slack channel for feedback and/or questions.

[Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[patch release guidelines]: https://docs.gitlab.com/ee/policy/maintenance.html#patch-releases
[GitLab project]: https://gitlab.com/gitlab-org/gitlab
[`e2e:package-and-test-ee` pipeline]: https://docs.gitlab.com/ee/development/testing_guide/end_to_end/package_and_test_pipeline.html
[Managed Versioning]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/index.md
[GitLab canonical project]: https://gitlab.com/gitlab-org/gitlab
[stable branch template]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Stable%20Branch.md
[severity label]: https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity
[test flakiness]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/test-metrics-dashboards/#package-and-test
[#releases]: https://gitlab.slack.com/archives/C0XM5UU6B
[#development]: https://gitlab.slack.com/archives/C02PF508L
[long lived environments]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/837
[cherry-pick feature]: https://docs.gitlab.com/ee/user/project/merge_requests/cherry_pick_changes.html#cherry-pick-all-changes-from-a-merge-request
[backport request]: https://gitlab.com/gitlab-org/release/tasks/-/issues/new?issuable_template=Backporting-request
[general guidelines]: #general-guidelines
[Backporting to versions outside of the Maintenance Policy]: #backporting-to-versions-outside-of-the-maintenance-policy
[backporting to older releases]: https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases
