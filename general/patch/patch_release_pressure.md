# Patch release pressure

Per the [Maintenance Policy], patch releases only include bug fixes for the current stable
released version of GitLab, this outlines the patch release pressure as the number of unreleased
merge requests merged into the current stable branch.

Release managers must use the patch release pressure information to determine if a patch release
is required, [other factors] may help direct this decision.

## Determine if a patch release is required

Two S1 fixes, or 10 overall fixes are both likely to [trigger a patch release],
although the final decision rests with the release managers.

To determine the number of fixes waiting to be patched:

1. Open up the [release manager dashboard].
1. Take a look at the "Patch release pressure: S1/S2" panel.
1. Search for the information associated with the current version.

**Example:** Assuming 15.11 is the current version and that there are two S1/S2 fixes waiting
to be patched for that version, release managers should highly consider doing a patch release for 15.11.

| Release Manager Dashboard |
| ------ |
| ![Patch release pressure](images/release_manager_dashboard_example.png) |

## Release manager dashboard

Patch release pressure information can be found in the [release manager dashboard]
in two panels:

* **S1/S2 pressure**: Represents the number of S1/S2 merge requests merged into stable branches
  that haven't been released.
* **Total pressure**: Represents the number of merge requests merged into stable branches that 
  haven't been released regardless of severity. This number is inclusive of the `S1/S2` pressure.

| Release Manager Dashboard |
| ------ |
| ![Patch release pressure](images/patch_release_pressure.png) |

[Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[other factors]: ./release_managers.md#when-to-perform-a-patch-release
[trigger a patch release]: ./release_managers.md#1-create-an-issue-to-track-the-patch-release
[release manager dashboard]: https://dashboards.gitlab.net/d/delivery-release_management/delivery-release-management?orgId=1&refresh=5m
